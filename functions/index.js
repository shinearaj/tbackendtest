const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');
var admin = require('firebase-admin');


// initialising express

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

var customMiddleWare = function(req, res, next) {
    console.log('Custom middle ware is listning')
    next()
}

app.use(customMiddleWare);


//connnecting with firebase database


admin.initializeApp(functions.config().firebase);

var db = admin.firestore();


const response = {};
const key1 = 'code';
const key2 = 'message';



app.get("/ashtray", function(req, res) {

    response["version"] = "5";

    res.json(response);

});


app.post("/addCustomer", function(req, res) {



    // fetching parametered from body

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const fatherName = req.body.fatherName;
    var panNumber = req.body.panNumber;
    const dateOfBirth = req.body.dateOfBirth;
    const gender = req.body.gender;
    const email = req.body.email;
    const address = req.body.address;
    const profileImage = req.body.profileImage;


    panNumber = panNumber.toUpperCase();

    // make sure all parameters are defined

    if (!firstName || !lastName || !fatherName || !panNumber || !dateOfBirth || !gender ||
        !email || !address || !profileImage) {


        res.json({
            success: false,
            message: 'All fields are mandatory'
        });


    } else {


        /*
         in a pan number , first 5 letters should be alphaabets
        second 4 should be number
        last one should be alphabet
        */

        const validPanReg = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;


        //check for pan validity

        if (!validPanReg.test(panNumber)) {


            res.json({
                success: false,
                message: 'Entered pan number is not valid'
            });

        }


        //dob validity

        if (!isValidDate(dateOfBirth)) {


            res.json({
                success: false,
                message: 'Dob should be in yy/mm/dd format'
            });

        }


        // checking whether pan number already exists or not in the database

        const customrRef = db.collection('customers').where('panNumber', '==', panNumber);


        customrRef.get()
            .then(snapshot => {

            if (snapshot.empty) {
            console.log('No matching documents.');


            var docRef = db.collection('customers');

            var customRes = '';

            const timestamp = +new Date;

            const key1 = 'cdfgpo';
            const key2 = 'hgtys';




            const token = encryptThis(key1 + panNumber + key2);


            docRef.add({
                firstName: firstName,
                lastName: lastName,
                fatherName: fatherName,
                panNumber: panNumber,
                dateOfBirth: dateOfBirth,
                gender: gender,
                email: email,
                address: address,
                profileImage: profileImage,
                token: token

            }).then(ref => {

                console.log('Added document with ID: ', ref.id);


            res.json({
                success: true,
                message: 'Added Succesfully',
                token: token
            });

            return null;


        }).
            catch(error => {
                console.log(error);

            res.json({
                success: false,
                message: error
            });

        });


        } else {

            res.json({
                success: false,
                message: 'Entered PAN Number already exists in dabata'
            });
        }


        return null;

    }).
        catch(error => {
            console.log(error);

        res.json({
            success: false,
            message: error
        });

    });


    }


    function isValidDate(dateString) {


        var regEx = /^\d{2}(\/)\d{2}(\/)\d{2}$/;
        return dateString.match(regEx) !== null;


    }

    function encryptThis(str) {
        return str.split("").reverse().join("");
    }




});


app.get("/getCustomerDetails", function(req, res) {


    var header = req.headers['authorization'];


    if (!header) {

        res.status(401).send('authorization token is missing');

    }


    var pan = decryptThis(header);


    console.log('pan is ' + pan);




    const customrRef = db.collection('customers').where('panNumber', '==', pan);



    customrRef.get().then(snapshot => {
        if (snapshot.empty) {
        console.log('No matching documents.');

        res.status(401).send('authorization failed');
        return;
    }

    snapshot.forEach(doc => {
        console.log(doc.id, '=>', doc.data());




    res.json({
        success: true,
        data: doc.data()
    });



});


    return null;

})
.catch(error => {
        console.log('Error getting documents', error);


    res.json({
        success: false,
        message: error
    });

});




    function decryptThis(str) {


        str =  str.substring(5);
        str = str.slice(0, -6);

        return str.split("").reverse().join("");
    }




});


exports.widgets = functions.https.onRequest(app);